import pandas as pd
from pprint import pprint

chemin_fichier = 'pivot-tables.xlsx'

donnees_sheet1 = pd.read_excel(chemin_fichier, sheet_name='Sheet1')

donnees_sheet1['Product'] = donnees_sheet1['Product'].str.lower().str.strip()
donnees_sheet1['Country'] = donnees_sheet1['Country'].str.lower().str.strip()
donnees_sheet1['Category'] = donnees_sheet1['Category'].str.lower().str.strip()

def tcd_par_pays(liste_pays):
    donnees_filtrees = donnees_sheet1[donnees_sheet1['Country'].isin(liste_pays)]
    tcd = pd.pivot_table(donnees_filtrees, values='Amount', index='Product', aggfunc='count').rename(columns={'Amount': 'Count of Amount'})
    tcd = tcd.sort_values(by='Count of Amount', ascending=False)
    tcd.loc['Grand Total'] = tcd.sum()
    
    return tcd

test_tcd = tcd_par_pays(['france', 'germany'])
pprint(test_tcd)

categories = ['Fruit','Vegetables'] 

def tcd_deux_dim(categories):
    if 'both' in categories:  
        donnees_filtrees = donnees_sheet1
    else:  
        donnees_filtrees = donnees_sheet1[donnees_sheet1['Category'].isin([cat.lower() for cat in categories])]    
    tcd_deux_dim = pd.pivot_table(donnees_filtrees, values='Amount', index='Country', columns='Product', aggfunc='sum', margins=True, margins_name='Grand Total')
    
    return tcd_deux_dim

test_tcd_2 = tcd_deux_dim(categories)
print(test_tcd_2)


