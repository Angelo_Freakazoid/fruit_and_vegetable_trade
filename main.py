import pandas as pd
import mysql.connector
from mysql.connector import Error
from pprint import pprint

chemin_fichier = 'pivot-tables.xlsx'
donnees_sheet1 = pd.read_excel(chemin_fichier, sheet_name='Sheet1')

for col in donnees_sheet1.select_dtypes(include=['object']).columns:
    donnees_sheet1[col] = donnees_sheet1[col].str.lower().str.strip()

donnees_sheet1['Date'] = donnees_sheet1['Date'].dt.strftime('%Y-%m-%d')

pprint(donnees_sheet1.head())

try:
    connection = mysql.connector.connect(
        host='localhost',
        user='alucard',
        password='coucou123',
        database='pivot'
    )
    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connecté au serveur MySQL version ", db_Info)
        cursor = connection.cursor()

        products = pd.DataFrame(donnees_sheet1[['Product', 'Category']].drop_duplicates())
        for index, row in products.iterrows():
            cursor.execute("INSERT INTO Products (product_name, category) VALUES (%s, %s) ON DUPLICATE KEY UPDATE product_name=product_name;", (row['Product'], row['Category']))
        connection.commit()
        
        orders = pd.DataFrame(donnees_sheet1[['Order ID', 'Date', 'Country']].drop_duplicates())
        for index, row in orders.iterrows():
            cursor.execute("INSERT INTO Orders (order_ID, order_date, country) VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE order_ID=order_ID;", (row['Order ID'], row['Date'], row['Country']))
        connection.commit()

        product_ids = {}
        cursor.execute("SELECT product_ID, product_name FROM Products;")
        for (product_ID, product_name) in cursor:
            product_ids[product_name] = product_ID

        for index, row in donnees_sheet1.iterrows():
            product_ID = product_ids.get(row['Product'].lower().strip())
            order_ID = row['Order ID']  
            amount = row['Amount']
            query = "INSERT INTO OrderDetails (order_ID, product_ID, amount) VALUES (%s, %s, %s);"
            cursor.execute(query, (order_ID, product_ID, amount))
        connection.commit()

except Error as e:
    print("Erreur lors de la connexion à MySQL", e)
finally:
    if connection.is_connected():
        cursor.close()
        connection.close()
        print("Connexion MySQL est fermée")
