DROP DATABASE IF EXISTS pivot;
CREATE DATABASE pivot;
USE pivot;

grant all privileges on pivot.* to 'alucard'@'localhost';

CREATE TABLE Products (
    product_ID INT PRIMARY KEY AUTO_INCREMENT,
    product_name VARCHAR(255),
    category VARCHAR(255)
);

CREATE TABLE Orders (
    order_ID INT PRIMARY KEY,
    order_date DATE,
    country VARCHAR(255)
);

CREATE TABLE OrderDetails (
    order_detail_ID INT PRIMARY KEY AUTO_INCREMENT,
    order_ID INT,
    product_ID INT,
    amount INT,
    FOREIGN KEY (order_ID) REFERENCES Orders(order_ID),
    FOREIGN KEY (product_ID) REFERENCES Products(product_ID)
);


